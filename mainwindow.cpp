#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "calc_item.h"
#include <QDebug>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    m_states.append("");
    connect(ui->actionExit, SIGNAL(triggered(bool)), qApp, SLOT(quit()));

    QPixmap pixmap1(":/images/images/Clear.png");
    QIcon icon1(pixmap1);
    ui->pushButtonClear->setIcon(icon1);
    ui->pushButtonClear->setIconSize(pixmap1.rect().size());

    QPixmap pixmap2(":/images/images/Undo.png");
    QIcon icon2(pixmap2);
    ui->pushButtonBack->setIcon(icon2);
    ui->pushButtonBack->setIconSize(pixmap2.rect().size());


    connect(ui->pushButton0, SIGNAL(clicked(bool)), this, SLOT(symbolClicked()));
    connect(ui->pushButton1, SIGNAL(clicked(bool)), this, SLOT(symbolClicked()));
    connect(ui->pushButton2, SIGNAL(clicked(bool)), this, SLOT(symbolClicked()));
    connect(ui->pushButton3, SIGNAL(clicked(bool)), this, SLOT(symbolClicked()));
    connect(ui->pushButton4, SIGNAL(clicked(bool)), this, SLOT(symbolClicked()));
    connect(ui->pushButton5, SIGNAL(clicked(bool)), this, SLOT(symbolClicked()));
    connect(ui->pushButton6, SIGNAL(clicked(bool)), this, SLOT(symbolClicked()));
    connect(ui->pushButton7, SIGNAL(clicked(bool)), this, SLOT(symbolClicked()));
    connect(ui->pushButton8, SIGNAL(clicked(bool)), this, SLOT(symbolClicked()));
    connect(ui->pushButton9, SIGNAL(clicked(bool)), this, SLOT(symbolClicked()));

    connect(ui->pushButtonPlus, SIGNAL(clicked(bool)), this, SLOT(symbolClicked()));
    connect(ui->pushButtonMinus, SIGNAL(clicked(bool)), this, SLOT(symbolClicked()));
    connect(ui->pushButtonMultiply, SIGNAL(clicked(bool)), this, SLOT(symbolClicked()));
    connect(ui->pushButtonDivide, SIGNAL(clicked(bool)), this, SLOT(symbolClicked()));
    connect(ui->pushButtonDecimal, SIGNAL(clicked(bool)), this, SLOT(symbolClicked()));

    connect(ui->pushButtonClear, SIGNAL(clicked(bool)), this, SLOT(clear()));
    connect(ui->pushButtonBack, SIGNAL(clicked(bool)), this, SLOT(back()));

    connect(ui->pushButtonCalculate, SIGNAL(clicked(bool)), this, SLOT(calculate()));
    connect(ui->lineEdit, SIGNAL(returnPressed()), this, SLOT(calculate()));

    connect(ui->lineEdit, SIGNAL(textChanged(QString)), this, SLOT(addStateToList()));

    connect(ui->actionAbout_Qt, SIGNAL(triggered(bool)), qApp, SLOT(aboutQt()));
    connect(ui->actionAbout, SIGNAL(triggered(bool)), this, SLOT(about()));

}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::about()
{
    QMessageBox::information(this, tr("About"), tr("This application Calculator was made by\n"
                                                   "Maria Grivko and Igor Grivko."));
}

void MainWindow::symbolClicked()
{
    QPushButton* buttonClicked = qobject_cast<QPushButton*>(sender());
    if(buttonClicked)
    {
        ui->lineEdit->setText(ui->lineEdit->text() + buttonClicked->text());
    }
    else
    {
        qDebug() << "Cannot get QPushButton* in digitClicked() slot.";
    }
}


void MainWindow::clear()
{
    ui->lineEdit->clear();
}

void MainWindow::addStateToList()
{
    statusBar()->clearMessage();
    m_states.append(ui->lineEdit->text());
}

void MainWindow::back()
{
    disconnect(ui->lineEdit, SIGNAL(textChanged(QString)), this, SLOT(addStateToList()));

    statusBar()->clearMessage();

    if(!m_states.isEmpty() && !(m_states.count()==1))
    {
        m_states.removeLast();
        ui->lineEdit->setText(m_states.last());
    }
    else
    {
        statusBar()->showMessage(tr("No undo history"));
    }
    connect(ui->lineEdit, SIGNAL(textChanged(QString)), this, SLOT(addStateToList()));

}

double MainWindow::calculateWithoutBrackets(QList<CalcItem> items)
{
    bool found = true;
    while (found)
    {
        found = false;
        for (int i = 0; i < items.count(); i++)
        {
            if (items[i].type() == CalcItem::Multiply)
            {
                double res = items[i-1].number() * items[i+1].number();
                items.removeAt(i+1);
                items.removeAt(i);
                items[i-1] = CalcItem(CalcItem::Number, res);
                found = true;
                break;
            }
        }
    }

    found = true;
    while (found)
    {
        found = false;
        for (int i = 0; i < items.count(); i++)
        {
            if (items[i].type() == CalcItem::Divide)
            {
                double res = items[i-1].number() / items[i+1].number();
                items.removeAt(i+1);
                items.removeAt(i);
                items[i-1] = CalcItem(CalcItem::Number, res);
                found = true;
                break;
            }
        }
    }
    found = true;
    while (found)
    {
        found = false;
        for (int i = 0; i < items.count(); i++)
        {
            if (items[i].type() == CalcItem::Plus)
            {
                double res = items[i-1].number() + items[i+1].number();
                items.removeAt(i+1);
                items.removeAt(i);
                items[i-1] = CalcItem(CalcItem::Number, res);
                found = true;
                break;
            }

        }
    }

    found = true;
    while (found)
    {
        found = false;
        for (int i = 0; i < items.count(); i++)
        {
            if (items[i].type() == CalcItem::Minus)
            {
                double res = items[i-1].number() - items[i+1].number();
                items.removeAt(i+1);
                items.removeAt(i);
                items[i-1] = CalcItem(CalcItem::Number, res);
                found = true;
                break;
            }

        }
    }

    return items[0].number();
}


void MainWindow::calculate()
{
    QString str;
    QString str1;
    str = ui->lineEdit->text();
    qDebug() << str;
    str = str.replace(" ", "");
    qDebug() << str;


    QList<CalcItem> items;

    bool correct_input = true;
    int last_position = 0;
    for (int i = 0; i < str.count(); i++)
    {
        if (str[i]=='+' || (str[i]=='-' && i!=0) ||str[i]=='*'|| str[i]=='/'
                || str[i] == '(' || str[i] == ')')
        {

            str1 = str.mid(last_position,i-last_position);
            if (str1 != "")
            {
                double d = str1.toDouble(&correct_input);
                if (correct_input == false)
                {
                    ui->lineEdit->setSelection(last_position,i-last_position);
                    statusBar()->showMessage(tr("Malformed expression"));
                    return;
                }
                items.append(CalcItem(CalcItem::Number, d));
            }




            if (str[i]=='+')
            {
                items.append(CalcItem(CalcItem::Plus));
            }
            if (str[i]=='-')
            {
                items.append(CalcItem(CalcItem::Minus));
            }
            if (str[i]=='*')
            {
                items.append(CalcItem(CalcItem::Multiply));
            }
            if (str[i]=='/')
            {
                items.append(CalcItem(CalcItem::Divide));
            }
            if (str[i]=='(')
            {
                items.append(CalcItem(CalcItem::LeftBracket));
            }
            if (str[i]==')')
            {
                items.append(CalcItem(CalcItem::RightBracket));
            }


            last_position = i+1;

        }
    }

    str1 = str.mid(last_position,str.count()-last_position);
    if (str1 != "")
    {
        double d = str1.toDouble(&correct_input);
        if (correct_input == false)
        {
            ui->lineEdit->setSelection(last_position,str.count()-last_position);
            statusBar()->showMessage(tr("Malformed expression"));
            return;
        }
        if (str1 != "")
        items.append(CalcItem(CalcItem::Number, d));
    }
    bool ok;
    double ch = calculateAll(items, &ok);
    if(ok)
        ui->lineEdit->setText(QString::number(ch));
}

double MainWindow::calculateAll(QList<CalcItem> items, bool* ok)
{
    bool found = true;
    while(found)
    {
        int leftIndex = -1;
        int rightIndex = -1;
        found = false;
        for(int i = items.count()-1; i >= 0 ; --i)
        {
            if(items[i].type() == CalcItem::LeftBracket)
            {
                leftIndex = i;
                for(int j = i+1; j < items.count(); ++j)
                {
                    if(items[j].type() == CalcItem::RightBracket)
                    {
                        rightIndex = j;
                        break;
                    }
                }
                if(rightIndex == -1)
                {
                    ui->lineEdit->setSelection(leftIndex,leftIndex +1);
                    statusBar()->showMessage(tr("Malformed expression"));
                    *ok = false;
                    return 0;
                }
                else
                {
                    break;
                }
            }
        }
        qDebug() << "leftIndex, rightIndex" << leftIndex << rightIndex;
        if(leftIndex != -1 && rightIndex != -1)
        {
            found = true;
            QList<CalcItem> items1;
            for(int j = leftIndex+1; j < rightIndex; ++j)
            {
                items1.append(items[j]);
            }
            double d = calculateWithoutBrackets(items1);
            qDebug() << "calculateWithoutBrackets" << d;
            //Delete items in brackets.
            for(int j = rightIndex; j > leftIndex; --j)
            {
                items.removeAt(j);
            }
            items[leftIndex] = CalcItem(CalcItem::Number, d);
        }
    }
    *ok = true;
    return calculateWithoutBrackets(items);
}
