#-------------------------------------------------
#
# Project created by QtCreator 2015-11-04T12:48:20
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = calculatoroncemore
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    calc_item.cpp

HEADERS  += mainwindow.h \
    calc_item.h

FORMS    += mainwindow.ui

RESOURCES += \
    resource.qrc
