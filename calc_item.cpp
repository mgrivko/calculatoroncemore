#include "calc_item.h"

CalcItem::CalcItem()
{
    m_type = CalcItem::Number;
    m_number = 0.0;
}

CalcItem::CalcItem(ItemType type, double number)
{
    m_type = type;
    m_number = number;
}


void CalcItem::setType(ItemType type)
{
    m_type = type;
}

CalcItem::ItemType CalcItem::type() const
{
    return m_type;
}

void CalcItem::setNumber(double d)
{
    m_number = d;
}

double CalcItem::number() const
{
    return m_number;
}
