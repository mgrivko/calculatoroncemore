#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "calc_item.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    enum Operation
    {
        Plus,
        Minus,
        Multiply,
        Divide
    };

private slots:

    void about();
    void clear();
    void back();

    void calculate();
    double calculateAll(QList<CalcItem> items, bool* ok);
    double calculateWithoutBrackets(QList<CalcItem> items);
    void symbolClicked();

    void addStateToList();


private:
    Ui::MainWindow *ui;
    Operation m_operation;
    QList<QString> m_states;
};

#endif // MAINWINDOW_H
