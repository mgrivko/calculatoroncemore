#ifndef CALCITEM_H
#define CALCITEM_H


class CalcItem
{
public:
    enum ItemType
    {
        Plus,
        Minus,
        Multiply,
        Divide,
        LeftBracket,
        RightBracket,
        Number
    };


    CalcItem();
    CalcItem(ItemType type, double number = 0.0);


    void setType(ItemType type);
    ItemType type() const;
    void setNumber(double d);
    double number() const;

private:
    ItemType m_type;
    double m_number;

};

#endif // CALCITEM_H
